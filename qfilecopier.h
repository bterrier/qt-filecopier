#ifndef QFILECOPIER_H
#define QFILECOPIER_H

#include <QObject>
#include <QRunnable>

class QFileCopierPrivate;
class QFileCopier : public QObject, public QRunnable
{
    Q_OBJECT

    Q_PROPERTY(qint64 progress READ progress WRITE setProgress NOTIFY progressChanged)

    Q_DECLARE_PRIVATE(QFileCopier)

public:
    enum FileCopyError {
        NoError = 0,
        CopyError = 1
    };


    explicit QFileCopier(QObject *parent = 0);
    ~QFileCopier();


    void run();

    int error();

    QString errorString() const;

    void setProgress(qint64 progress);
    qint64 progress() const;

    static QFileCopier *copy(const QString &fileName, const QString &newName);

public Q_SLOTS:
    bool copy();

Q_SIGNALS:
    void progressChanged(qint64 progress);
    void copyProgress(qint64 bytesCopied, qint64 bytesTotal);
    void finished();

private:
    Q_DISABLE_COPY(QFileCopier)
    QScopedPointer<QFileCopierPrivate> d_ptr;

};

#endif // QFILECOPIER_H
