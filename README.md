# QtFileCopier
## Introduction
QtFileCopier is a library which aims to provide utility classes to watch 
progress of a file copy.

It is currently a work in progress.

QtFileCopier is not part of Qt and is not endorsed by The Qt Company in any way.

## Build
QtFileCopier should work with any Qt 5 version.
You can either include the provided .h and .cpp files in your project or use the
.pro file to build a library.