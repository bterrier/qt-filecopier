#include "qfilecopier.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QTemporaryFile>
#include <QThread>

class QFileCopierPrivate {
    Q_DECLARE_PUBLIC(QFileCopier)
    QFileCopier *q_ptr = nullptr;
    QString source;
    QString target;
    int error = QFileCopier::NoError;
    QString errorString;
    qint64 progress;
    qint64 size;
    QThread *thread = nullptr;

    void setError(int err, const QString &message)
    {
        error = err;
        errorString = message;
    }
    void unsetError(){}
};

QFileCopier::QFileCopier(QObject *parent) :
    QObject(parent),
    d_ptr(new QFileCopierPrivate)
{

}

QFileCopier::~QFileCopier()
{

}

/*!
    Start the copy after moving the object to the current thread.
    This function is meant to be called when using this object as
    a QRunnable in a QThreadPool.
*/
void QFileCopier::run()
{
    // Move this object to current running thread
    QThread *currentThread = QThread::currentThread();
    QThread *affinityThread = thread();
    if (currentThread
            && affinityThread != currentThread)
    {
        if (affinityThread) {
            const bool status = QMetaObject::invokeMethod(this,
                                                          "moveToThread",
                                                          Qt::BlockingQueuedConnection,
                                                          Q_ARG(QThread *, currentThread)
                                                          );

            if (!status)
            {
                qWarning("QFileCopier::run: Faile to move object to running thread");
            }
        } else {
            moveToThread(currentThread);
        }
    }

    copy();
}

bool QFileCopier::copy()
{
    Q_D(QFileCopier);
    if (d->source.isEmpty()) {
        qWarning("QFileCopier::copy: Empty or null source file name");
        return false;
    }

    if (d->target.isEmpty()) {
        qWarning("QFileCopier::copy: Empty or null target file name");
        return false;
    }
    if (QFile::exists(d->target)) {
        // ### Race condition. If a file is moved in after this, it /will/ be
        // overwritten. On Unix, the proper solution is to use hardlinks:
        // return ::link(old, new) && ::remove(old); See also rename().
        d->setError(QFile::CopyError, tr("Destination file exists"));
        return false;
    }
    d->unsetError();

    if(error() == QFile::NoError) {

        bool error = false;

        QFile sourceFile(d->source);
        if(!sourceFile.open(QFile::ReadOnly)) {
            error = true;
            d->setError(QFile::CopyError, tr("Cannot open %1 for input").arg(d->source));
        } else {
            QString fileTemplate = QLatin1String("%1/qt_temp.XXXXXX");
#ifdef QT_NO_TEMPORARYFILE
            QFile out(fileTemplate.arg(QFileInfo(newName).path()));
            if (!out.open(QIODevice::ReadWrite))
                error = true;
#else
            QTemporaryFile out(fileTemplate.arg(QFileInfo(d->target).path()));
            if (!out.open()) {
                out.setFileTemplate(fileTemplate.arg(QDir::tempPath()));
                if (!out.open())
                    error = true;
            }
#endif
            if (error) {
                out.close();
                sourceFile.close();
                d->setError(QFile::CopyError, tr("Cannot open for output"));
            } else {
                char block[4096];
                qint64 totalRead = 0;
                d->size = sourceFile.size();
                while(!sourceFile.atEnd()) {
                    qint64 in = sourceFile.read(block, sizeof(block));
                    if (in <= 0)
                        break;
                    totalRead += in;
                    if(in != out.write(block, in)) {
                        sourceFile.close();
                        d->setError(QFile::CopyError, tr("Failure to write block"));
                        error = true;
                        break;
                    }

                    setProgress(totalRead);
                }

                if (totalRead != sourceFile.size()) {
                    // Unable to read from the source. The error string is
                    // already set from read().
                    error = true;
                }
                if (!error && !out.rename(d->target)) {
                    error = true;
                    sourceFile.close();
                    d->setError(QFile::CopyError, tr("Cannot create %1 for output").arg(d->target));
                }
#ifdef QT_NO_TEMPORARYFILE
                if (error)
                    out.remove();
#else
                if (!error)
                    out.setAutoRemove(false);
#endif
            }
        }
        if(!error) {
            QFile::setPermissions(d->target, sourceFile.permissions());
            sourceFile.close();
            d->unsetError();
            return true;
        }

    }
    return false;
}

int QFileCopier::error()
{
    Q_D(const QFileCopier);
    return d->error;
}

QString QFileCopier::errorString() const {
    Q_D(const QFileCopier);
    return d->errorString;
}

void QFileCopier::setProgress(qint64 progress)
{
    Q_D(QFileCopier);
    if (d->progress == progress)
        return;

    d->progress = progress;
    emit progressChanged(d->progress);
    emit copyProgress(d->progress, d->size);
}

qint64 QFileCopier::progress() const {
    Q_D(const QFileCopier);
    return d->progress;
}

QFileCopier *QFileCopier::copy(const QString &fileName, const QString &newName)
{
    QFileCopier *copier = new QFileCopier();
    return copier;
}
